.PHONY: all clean run

all: 
	cd kernel && $(MAKE)

clean:
	find . -type f \( -name '*.o' -o -name '*.elf' \) -exec rm {} +

run: all
	qemu-system-i386 -kernel kernel.elf
