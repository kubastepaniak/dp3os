#include "AsmUtil.hpp"
#include "Console.hpp"
#include "CPUIO.hpp"
#include "CppAllocator.hpp"
#include "Echo.hpp"
#include "GDTManager.hpp"
#include "IDTManager.hpp"
#include "KeyboardDriver.hpp"
#include "multiboot.h"
#include "PICDriver.hpp"
#include "Util.hpp"

#include "paging.hpp"
#include "Scheduler.hpp"

char memory[1024];

void setInitialCppAllocatorImplementation() {
    CppAllocator::setNewFunction([](unsigned int size) -> void* {
        static int position = 0;
        void* retval = memory + position;
        position += size;
        return retval;
    });
}

extern "C" void kernelMain(multiboot_info* mbt) {
    setInitialCppAllocatorImplementation();
    AsmUtil::cli();
    GDTManager::init();
    PagingManager::initialise_paging(mbt->mem_upper * 1024);
    PICDriver::init();
    IDTManager::init();
    KeyboardDriver::init(&Echo::keyPressed);
    Scheduler::init();
    AsmUtil::sti();
    Console::clear();
    Console::printString("DP3OS\n");
    multiboot_memory_map_t* mmap = (multiboot_memory_map_t*)mbt->mmap_addr;					//Taking information about the memory size from GRUB
    while((uint32_t)mmap < mbt->mmap_addr + mbt->mmap_length) {
        Console::printString("\nAddr_h: ");
        Console::printInt(mmap->addr_high, 16);
        Console::printString("  Addr_l: ");
        Console::printInt(mmap->addr_low, 16);
        Console::printString("  Length_h: ");
        Console::printInt(mmap->len_high, 16);
        Console::printString("  Length_l: ");
        Console::printInt(mmap->len_low, 16);
        Console::printString("  Type: ");
        Console::printInt(mmap->type, 16);
        mmap = (multiboot_memory_map_t*) ((unsigned int)mmap + mmap->size + sizeof(mmap->size));
    }
    
    Console::printString("\nAvailable memory: ");
    Console::printInt(mbt->mem_upper/1024, 10);
    Console::printString(" MiB\n");
    uint32_t *ptr = (uint32_t*)0x80BFFD;			//0x80BFFC does not trigger page fault
    uint32_t do_page_fault = *ptr;
    while(1) AsmUtil::hlt();
}
