#include "CircularBuffer.hpp"

#include "AsmUtil.hpp"

CircularBuffer::CircularBuffer(): size(SIZE) {
    readIndex = 0;
    writeIndex = 0;
}

int CircularBuffer::writeByte(char byte) {
    if((writeIndex+1)%size == readIndex) {
        //Full buffer
        return -1;
    } else {
        buffer[writeIndex] = byte;
        writeIndex = (writeIndex+1)%size;
        return 0;
    }
}

char CircularBuffer::readByte() {
    while(readIndex == writeIndex) {
        //Empty buffer
        AsmUtil::hlt();
    }
    char result = buffer[readIndex];
    readIndex = (readIndex+1)%size;
    return result;
}
