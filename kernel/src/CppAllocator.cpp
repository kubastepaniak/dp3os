#include "CppAllocator.hpp"

CppAllocator::NewFunction CppAllocator::newFunction;
CppAllocator::DeleteFunction CppAllocator::deleteFunction;

void* operator new(unsigned int size) {
    return CppAllocator::newFunction(size);
}

void operator delete(void* pointer) {
    CppAllocator::deleteFunction(pointer);
}

namespace std {
    void __throw_bad_alloc() {
        
    }
}

void CppAllocator::setNewFunction(NewFunction newFunction) {
    CppAllocator::newFunction = newFunction;
}

void CppAllocator::setDeleteFunction(DeleteFunction deleteFunction) {
    CppAllocator::deleteFunction = deleteFunction;
}
