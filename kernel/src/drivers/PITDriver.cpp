#include "PITDriver.hpp"

void PITDriver::init(int frequency, FPIH handler) {
	PICDriver::registerIRQhandler(0, handler);

	uint32_t divisor = 1193180 / frequency;

	CPUIO::writePort(0x43, 0x36);

	uint8_t low = (uint8_t)(divisor & 0xFF);
	uint8_t high = (uint8_t)((divisor >> 8) & 0xFF);

	CPUIO::writePort(0x40, low);
	CPUIO::writePort(0x40, high);
}
