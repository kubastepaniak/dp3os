#include "KeyboardDriver.hpp"

#include "CPUIO.hpp"
#include "Console.hpp"
#include "IDTManager.hpp"
#include "PICDriver.hpp"

KeyboardDriver::FPUH KeyboardDriver::userHandler;

void KeyboardDriver::init(FPUH handler) {
    userHandler = handler;
    PICDriver::registerIRQhandler(1, &keyboardEventHandler);
}

void KeyboardDriver::keyboardEventHandler() {
    userHandler(CPUIO::readPort(dataPort));
}
