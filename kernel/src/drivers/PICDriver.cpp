#include "PICDriver.hpp"

#include "Console.hpp"
#include "CPUIO.hpp"
#include "IDTManager.hpp"

PICDriver::FPIH PICDriver::irqHandlers[16] = {};

void PICDriver::setIRQmask(uint8_t irq) {
        uint8_t port;
    uint8_t offset = irq;
    
    if(irq <= 7) {
        port = PIC1_DATA;
    } else if(irq <= 15) {
        port = PIC2_DATA;
        offset -= 8;
    } else {
        // IRQ out of range TODO:Send error message
    }
    
    unsigned char currentMask = CPUIO::readPort(port);

    CPUIO::writePort(port, currentMask | (1<<offset));
}

void PICDriver::clearIRQmask(uint8_t irq) {
    uint8_t port;
    uint8_t offset = irq;
    
    if(irq <= 7) {
        port = PIC1_DATA;
    } else if(irq <= 15) {
        port = PIC2_DATA;
        offset -= 8;
    } else {
        // IRQ out of range TODO:Send error message
    }
    
    unsigned char currentMask = CPUIO::readPort(port);

    CPUIO::writePort(port, currentMask & ~(1<<offset));
}

void PICDriver::init() {
        /*     Ports
    *     PIC1    PIC2
    *Command 0x20    0xA0
    *Data     0x21    0xA1
    */

    /* ICW1 - begin initialization */
    CPUIO::writePort(PIC1_COMMAND , 0x11);
    CPUIO::writePort(PIC2_COMMAND , 0x11);

    /* ICW2 - remap offset address of IDT */
    /*
    * In x86 protected mode, we have to remap the PICs beyond 0x20 because
    * Intel have designated the first 32 interrupts as "reserved" for cpu exceptions
    */
    CPUIO::writePort(PIC1_DATA , 0x20);
    CPUIO::writePort(PIC2_DATA , 0x28);

    /* ICW3 - setup cascading */
    CPUIO::writePort(PIC1_DATA , 0x00);
    CPUIO::writePort(PIC2_DATA , 0x00);

    /* ICW4 - environment info */
    CPUIO::writePort(PIC1_DATA , 0x01);
    CPUIO::writePort(PIC2_DATA , 0x01);
    /* Initialization finished */

    /* mask interrupts */
    CPUIO::writePort(PIC1_DATA , 0xff);
    CPUIO::writePort(PIC2_DATA , 0xff);
    
    clearIRQmask(2); //Enable interrupts from PIC2
}

void PICDriver::IRQ(uint8_t index) {
    uint8_t irq;
    if(PIC1_IRQ0 <= index && index <= PIC1_IRQ0+7) {
        irq = index - PIC1_IRQ0;
    } else if(PIC2_IRQ8 <= index && index <= PIC2_IRQ8+7) {
        irq = index - PIC2_IRQ8 + 8;
    } else {
        //Not an irq
    }
    
    if(irqHandlers[irq] != 0) {
        irqHandlers[irq]();
    } else {
        //Irq not registered. TODO: Display error message
    }
    
    if(irq >= 8) {
        CPUIO::writePort(PIC2_COMMAND, PIC_EOI);
    }
    CPUIO::writePort(PIC1_COMMAND, PIC_EOI);
}

void PICDriver::registerIRQhandler(uint8_t irq, FPIH handler) {
    uint8_t offset = irq <= 7 ? PIC1_IRQ0 : PIC2_IRQ8 - 8;
    IDTManager::registerIntHandler(irq + offset, IRQ);
    irqHandlers[irq] = handler;
    clearIRQmask(irq);
}
void PICDriver::unregisterIRQhandler(uint8_t irq) {
    uint8_t offset = irq <= 7 ? PIC1_IRQ0 : PIC2_IRQ8 - 8;
    setIRQmask(irq);
    IDTManager::unregisterIntHandler(irq + offset);
    irqHandlers[irq] = 0;
}
