#include "Process.hpp"

Process::Process(char* name, int id, int priority, page_directory_t* page_directory, void (*function)(), State state, Process* parent) {
	this->name = name;
	this->id = id;
	this->priority = priority;
	this->state = state;
	this->parent = parent;
	this->page_directory = page_directory;
	this->function = function;

	this->context->eip = reinterpret_cast<uint32_t>(function);
}

page_directory_t* Process::getPageDirectory() {
	return this->page_directory;
}

volatile Context* Process::getContext() {
	return this->context;
}

int Process::getPriority() {
	return this->priority;
}
