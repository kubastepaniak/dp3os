#include "Scheduler.hpp"

Scheduler::Scheduler() {}

void Scheduler::init() {
	processes = new PriorityQueue<Process>();
	Process* first_process = new Process("idle", 0, 0, PagingManager::getCurrentDirectory(), &Scheduler::idleTask);
	processes->add(first_process);
	PITDriver::init(40, &Scheduler::switchToNext);
}

void Scheduler::switchToNext() {
	Process previous_process = processes->getCurrent();
	if (processes->hasNext() && processes->getNext().getPriority() != 0) {
		processes->next();
		Process new_process = processes->getCurrent();
		switch_process(previous_process.getContext(), new_process.getContext());
	}
}

PriorityQueue<Process>* Scheduler::getProcesses() {
	return processes;
}

void Scheduler::idleTask() {
	while (1) AsmUtil::hlt();
}

extern "C" {

uint32_t get_context_address(uint32_t pid) {
	return reinterpret_cast<uint32_t>(Scheduler::getProcesses()->getItem(pid).getContext());
}

uint32_t get_process_cr3(uint32_t pid) {
	return reinterpret_cast<uint32_t>(Scheduler::getProcesses()->getItem(pid).getPageDirectory()->physicalAddr);
}

}

