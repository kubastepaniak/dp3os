#include "IDTManager.hpp"

#include "AsmUtil.hpp"
#include "Console.hpp"
#include "CPUIO.hpp"
#include "ISR.hpp"

#define KERNEL_CODE_SEGMENT_OFFSET 0x08
#define INTERRUPT_GATE 0x8e


IDTManager::IntDesc IDTManager::IDT[idtSize];
IDTManager::FPIH IDTManager::intHandlers[idtSize] = {};

void IDTManager::init() {
    setIntDesc(0x00, int_00h);
    setIntDesc(0x01, int_01h);
    setIntDesc(0x02, int_02h);
    setIntDesc(0x03, int_03h);
    setIntDesc(0x04, int_04h);
    setIntDesc(0x05, int_05h);
    setIntDesc(0x06, int_06h);
    setIntDesc(0x07, int_07h);
    setIntDesc(0x08, int_08h);
    setIntDesc(0x09, int_09h);
    setIntDesc(0x0A, int_0Ah);
    setIntDesc(0x0B, int_0Bh);
    setIntDesc(0x0C, int_0Ch);
    setIntDesc(0x0D, int_0Dh);
    setIntDesc(0x0E, int_0Eh);
    setIntDesc(0x0F, int_0Fh);
    setIntDesc(0x10, int_10h);
    setIntDesc(0x11, int_11h);
    setIntDesc(0x12, int_12h);
    setIntDesc(0x13, int_13h);
    setIntDesc(0x14, int_14h);
    setIntDesc(0x15, int_15h);
    setIntDesc(0x16, int_16h);
    setIntDesc(0x17, int_17h);
    setIntDesc(0x18, int_18h);
    setIntDesc(0x19, int_19h);
    setIntDesc(0x1A, int_1Ah);
    setIntDesc(0x1B, int_1Bh);
    setIntDesc(0x1C, int_1Ch);
    setIntDesc(0x1D, int_1Dh);
    setIntDesc(0x1E, int_1Eh);
    setIntDesc(0x1F, int_1Fh);
    setIntDesc(0x20, int_20h);
    setIntDesc(0x21, int_21h);
    setIntDesc(0x22, int_22h);
    setIntDesc(0x23, int_23h);
    setIntDesc(0x24, int_24h);
    setIntDesc(0x25, int_25h);
    setIntDesc(0x26, int_26h);
    setIntDesc(0x27, int_27h);
    setIntDesc(0x28, int_28h);
    setIntDesc(0x29, int_29h);
    setIntDesc(0x2A, int_2Ah);
    setIntDesc(0x2B, int_2Bh);
    setIntDesc(0x2C, int_2Ch);
    setIntDesc(0x2D, int_2Dh);
    setIntDesc(0x2E, int_2Eh);
    setIntDesc(0x2F, int_2Fh);
    loadIDT();
}

void IDTManager::loadIDT() {
    IDTlocator idtLocator;
    idtLocator.limit = (sizeof(IntDesc) * IDT_SIZE) - 1;
    idtLocator.base = (unsigned int)&IDT;
    AsmUtil::loadIDT(&idtLocator);
}

IDTManager::IntDesc IDTManager::getIntDesc(uint8_t index) {
    return IDT[index];
}

void IDTManager::setIntDesc(uint8_t index, IntDesc descriptor) {
    IDT[index] = descriptor;
}

void IDTManager::setIntDesc(uint8_t index, void(*isr)()) {
    IntDesc intDesc;
    uint32_t isrAddr = reinterpret_cast<uint32_t>(isr);
    intDesc.offset_lowerbits = isrAddr & 0xffff;
    intDesc.selector = KERNEL_CODE_SEGMENT_OFFSET;
    intDesc.zero = 0;
    intDesc.type_attr = INTERRUPT_GATE;
    intDesc.offset_higherbits = (isrAddr & 0xffff0000) >> 16;
    setIntDesc(index, intDesc);
}

void IDTManager::interruptHandler(uint8_t index) {
    if(intHandlers[index] != 0) {
        intHandlers[index](index);
    } else {
        Console::printString("No handler registered for int 0x");
        Console::printInt(index, 16);
        Console::printString("\n");
    }
}

void IDTManager::registerIntHandler(uint8_t index, FPIH handler) {
    intHandlers[index] = handler;
}

void IDTManager::unregisterIntHandler(uint8_t index) {
    intHandlers[index] = 0;
}
