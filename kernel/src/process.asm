global switch_process
extern get_context_address
extern get_process_cr3

%macro save_context 0
    	push ebp
    	push edi
    	push esi
	push edx
	push ecx
    	push ebx
	push eax
%endmacro

%macro restore_context 0
	pop eax
	pop ebx
	pop ecx
	pop edx
	pop esi
	pop edi
	pop ebp
%endmacro

switch_process:
    push eax

    mov eax, ss
    push eax
    mov eax, esp
    push eax

    pushfd				
    mov eax, cs
    push eax
    lea eax, [resume_rip]
    push eax

    push 0

    save_context

    push edi
    push esi
    call get_context_address
    pop esi
    pop edi
    mov [eax], esp

    push edi
    push esi
    mov edi, esi
    call get_process_cr3
    pop esi
    pop edi
    mov cr3, eax

    push edi
    push esi
    mov edi, esi
    call get_context_address
    pop esi
    pop edi
    mov esp, [eax]

    restore_context

    add esp, 8

    iretd

resume_rip:
    pop eax
    pop eax
    ret

