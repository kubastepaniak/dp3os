#include "kmalloc.hpp"

extern uint32_t end;
uint32_t placement_address = (uint32_t)&end;

uint32_t kmalloc(uint32_t size, int align, uint32_t *phys)  //Returns the address of alocated memory//Align is for page alignment//*phys is the physical address of the allocated memory
{
  if (align == 1 && (placement_address & 0xFFFFF000)) // If the address is not already page-aligned
  {
    // Align it.
    placement_address &= 0xFFFFF000;		      //We have to define the placement_address
    placement_address += 0x1000;
  }
  if (phys)
  {
    *phys = placement_address;
  }
  uint32_t tmp = placement_address;
  placement_address += size;
  return tmp;
} 

