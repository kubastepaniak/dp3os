#include "Util.hpp"

#include "Console.hpp"

void* Util::memset(void* p, int i, int length) {
	unsigned char* dest = (unsigned char*) p;
	while(length > 0) {
		*dest = (unsigned char) i;
		dest++;
		length--;
	}
	return p;
}

