extern _ZN10IDTManager16interruptHandlerEh
global int_00h
global int_01h
global int_02h
global int_03h
global int_04h
global int_05h
global int_06h
global int_07h
global int_08h
global int_09h
global int_0Ah
global int_0Bh
global int_0Ch
global int_0Dh
global int_0Eh
global int_0Fh
global int_10h
global int_11h
global int_12h
global int_13h
global int_14h
global int_15h
global int_16h
global int_17h
global int_18h
global int_19h
global int_1Ah
global int_1Bh
global int_1Ch
global int_1Dh
global int_1Eh
global int_1Fh
global int_20h
global int_21h
global int_22h
global int_23h
global int_24h
global int_25h
global int_26h
global int_27h
global int_28h
global int_29h
global int_2Ah
global int_2Bh
global int_2Ch
global int_2Dh
global int_2Eh
global int_2Fh

int_00h:
    mov eax, 0x00
    jmp end_isr
    
int_01h:
    mov eax, 0x01
    jmp end_isr
    
int_02h:
    mov eax, 0x02
    jmp end_isr
    
int_03h:
    mov eax, 0x03
    jmp end_isr
    
int_04h:
    mov eax, 0x04
    jmp end_isr
    
int_05h:
    mov eax, 0x05
    jmp end_isr
    
int_06h:
    mov eax, 0x06
    jmp end_isr
    
int_07h:
    mov eax, 0x07
    jmp end_isr
    
int_08h:
    mov eax, 0x08
    jmp end_isr
    
int_09h:
    mov eax, 0x09
    jmp end_isr
    
int_0Ah:
    mov eax, 0x0A
    jmp end_isr
    
int_0Bh:
    mov eax, 0x0B
    jmp end_isr
    
int_0Ch:
    mov eax, 0x0C
    jmp end_isr
    
int_0Dh:
    mov eax, 0x0D
    jmp end_isr
    
int_0Eh:
    mov eax, 0x0E
    jmp end_isr
    
int_0Fh:
    mov eax, 0x0F
    jmp end_isr
    
int_10h:
    mov eax, 0x10
    jmp end_isr
    
int_11h:
    mov eax, 0x11
    jmp end_isr
    
int_12h:
    mov eax, 0x12
    jmp end_isr
    
int_13h:
    mov eax, 0x13
    jmp end_isr
    
int_14h:
    mov eax, 0x14
    jmp end_isr
    
int_15h:
    mov eax, 0x15
    jmp end_isr
    
int_16h:
    mov eax, 0x16
    jmp end_isr
    
int_17h:
    mov eax, 0x17
    jmp end_isr
    
int_18h:
    mov eax, 0x18
    jmp end_isr
    
int_19h:
    mov eax, 0x19
    jmp end_isr
    
int_1Ah:
    mov eax, 0x1A
    jmp end_isr
    
int_1Bh:
    mov eax, 0x1B
    jmp end_isr
    
int_1Ch:
    mov eax, 0x1C
    jmp end_isr
    
int_1Dh:
    mov eax, 0x1D
    jmp end_isr
    
int_1Eh:
    mov eax, 0x1E
    jmp end_isr
    
int_1Fh:
    mov eax, 0x1F
    jmp end_isr
    
int_20h:
    mov eax, 0x20
    jmp end_isr
    
int_21h:
    mov eax, 0x21
    jmp end_isr
    
int_22h:
    mov eax, 0x22
    jmp end_isr
    
int_23h:
    mov eax, 0x23
    jmp end_isr
    
int_24h:
    mov eax, 0x24
    jmp end_isr
    
int_25h:
    mov eax, 0x25
    jmp end_isr
    
int_26h:
    mov eax, 0x26
    jmp end_isr
    
int_27h:
    mov eax, 0x27
    jmp end_isr
    
int_28h:
    mov eax, 0x28
    jmp end_isr
    
int_29h:
    mov eax, 0x29
    jmp end_isr
    
int_2Ah:
    mov eax, 0x2A
    jmp end_isr
    
int_2Bh:
    mov eax, 0x2B
    jmp end_isr
    
int_2Ch:
    mov eax, 0x2C
    jmp end_isr
    
int_2Dh:
    mov eax, 0x2D
    jmp end_isr
    
int_2Eh:
    mov eax, 0x2E
    jmp end_isr
    
int_2Fh:
    mov eax, 0x2F
    jmp end_isr
    
end_isr:
    pushad
    cld ;Why is this needed here?
    push eax
    call _ZN10IDTManager16interruptHandlerEh ;void IDTManager::interruptHandler(uint8_t index);
    add esp, 4;
    popad
    iretd
