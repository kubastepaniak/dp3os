global _start
extern kernelMain

_start:
jmp mb1_end
align 0x10
mb2_start:
    dd 0xe85250d6                ; magic number
    dd 0                         ; protected mode code
    dd mb2_end - mb2_start ; header length

    ; checksum
    dd 0x100000000 - (0xe85250d6 + 0 + (mb2_end - mb2_start))

    ; required end tag
    dw 0    ; type
    dw 0    ; flags
    dd 8    ; size
mb2_end:

align 0x10
mb1_start:
    dd 0x1BADB002                ; magic number
    dd 0                         ; flags
    ; checksum
    dd 0x100000000 - (0x1BADB002 + 0)

mb1_end:

push ebx; for mmap
call kernelMain

unreachableKernelEnd:
jmp unreachableKernelEnd
