#include "GDTManager.hpp"

#include "AsmUtil.hpp"
#include "Console.hpp"

GDTManager::GDTentry GDTManager::GDT[gdtSize];

void GDTManager::init() {
    GDT[0] = translateFromSimple({0, 0, 0, 0});
    GDT[1] = translateFromSimple({0, 0xffffffff, 0x9A, 0xC});
    GDT[2] = translateFromSimple({0, 0xffffffff, 0x92, 0xC});
    GDTlocator gdtptr;
    gdtptr.size = (sizeof(GDTentry)*gdtSize) - 1;
    gdtptr.offset = reinterpret_cast<uint32_t>(&GDT);
    AsmUtil::loadGDT(&gdtptr);
    AsmUtil::reloadSegments();
}

GDTManager::GDTentry GDTManager::translateFromSimple(simpleGDTentry sGDTe) {
    GDTentry GDTe;
    
    GDTe.limit_0_7 = sGDTe.limit & 0xFF;
    GDTe.limit_8_15 = (sGDTe.limit>>8) & 0xFF;
    
    GDTe.flags_limit_16_19 = ((sGDTe.flags <<  4) & 0xF0) |
                             ((sGDTe.limit >> 16) & 0x0F);
    
    GDTe.base_0_7   =  sGDTe.base & 0xFF;
    GDTe.base_8_15  = (sGDTe.base >> 8)  & 0xFF;
    GDTe.base_16_23 = (sGDTe.base >> 16) & 0xFF;
    GDTe.base_24_31 = (sGDTe.base >> 24) & 0xFF;
    
    GDTe.accessByte = sGDTe.accessByte;
    return GDTe;
}
