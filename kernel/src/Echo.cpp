#include "Echo.hpp"

#include "Console.hpp"
#include "KeyboardMap.hpp"


bool Echo::alt;
bool Echo::shift;

void Echo::keyPressed(unsigned char keycode) {
        if(keycode & 0x80) {											//Key released
		keycode &= ~(0x80);
		if(keycode == KeyboardMap::KEY_ALT) {
			alt = false;
		}
		if(keycode == KeyboardMap::KEY_LEFT_SHIFT || keycode == KeyboardMap::KEY_RIGHT_SHIFT) {
			shift = false;
		}							
	}
	else {													//Key pressed
		if(keycode == KeyboardMap::KEY_ALT) {
			alt = true;
		}
		else if(keycode == KeyboardMap::KEY_LEFT_SHIFT || keycode == KeyboardMap::KEY_RIGHT_SHIFT) {
			shift = true;
		}
		else if(keycode == KeyboardMap::KEY_BACKSPACE) {
			Console::printChar('\b');
		}
		else {
			char key = shift ? KeyboardMap::uppercase[keycode] : KeyboardMap::lowercase[keycode];

			if(key) {
				Console::printChar(key);
			}
										
	}
    }
}
