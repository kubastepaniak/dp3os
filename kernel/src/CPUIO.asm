global _ZN5CPUIO8readPortEt
global _ZN5CPUIO9writePortEth

;dx - port number
_ZN5CPUIO8readPortEt: ;uint8_t readPort(uint16_t port)
    mov edx, [esp + 4]
    in al, dx
    ret

;dx - port number,al - data
_ZN5CPUIO9writePortEth: ;void writePort(uint16_t port, uint8_t data)
    mov   edx, [esp + 4]    
    mov   al, [esp + 4 + 4]  
    out   dx, al  
    ret
