#include "MemoryBlock.hpp"

MemoryBlock::MemoryBlock() : baseAddress(0), length(0) {}
MemoryBlock::MemoryBlock(uint32_t baseAddress, uint32_t length) : baseAddress(baseAddress), length(length) {}
MemoryBlock::operator void*() {
    return reinterpret_cast<void*>(baseAddress);
}
