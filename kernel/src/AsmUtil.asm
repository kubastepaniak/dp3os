global _ZN7AsmUtil7loadGDTEPN10GDTManager10GDTlocatorE ;void AsmUtil::loadGDT(GDTManager::GDTlocator*);
global _ZN7AsmUtil7loadIDTEPN10IDTManager10IDTlocatorE ;void AsmUtil::loadIDT(IDTManager::IDTlocator*);
global _ZN7AsmUtil8storeGDTEPN10GDTManager10GDTlocatorE ;void AsmUtil::storeGDT(GDTManager::GDTlocator*);
global _ZN7AsmUtil8storeIDTEPN10IDTManager10IDTlocatorE ;void AsmUtil::storeIDT(IDTManager::IDTlocator*);
global _ZN7AsmUtil14reloadSegmentsEv ;void AsmUtil::reloadSegments();
global _ZN7AsmUtil3cliEv ;void AsmUtil::cli();
global _ZN7AsmUtil3stiEv ;void AsmUtil::sti();
global _ZN7AsmUtil3hltEv ;void AsmUtil::hlt();

_ZN7AsmUtil7loadGDTEPN10GDTManager10GDTlocatorE: ;void AsmUtil::loadGDT(GDTManager::GDTlocator*);
    mov edx, [esp + 4]
    lgdt [edx]
    ret

_ZN7AsmUtil7loadIDTEPN10IDTManager10IDTlocatorE: ;void AsmUtil::loadIDT(IDTManager::IDTlocator*);
    mov edx, [esp + 4]
    lidt [edx]
    ret

_ZN7AsmUtil8storeGDTEPN10GDTManager10GDTlocatorE: ;void AsmUtil::storeGDT(GDTManager::GDTlocator*);
    mov edx, [esp + 4]
    sgdt [edx]
    ret
    
_ZN7AsmUtil8storeIDTEPN10IDTManager10IDTlocatorE: ;void AsmUtil::storeIDT(IDTManager::IDTlocator*);
    mov edx, [esp + 4]
    sgdt [edx]
    ret

_ZN7AsmUtil14reloadSegmentsEv: ;void AsmUtil::reloadSegments();
   JMP   0x08:reload_CS
reload_CS:
   MOV   AX, 0x10
   MOV   DS, AX
   MOV   ES, AX
   MOV   FS, AX
   MOV   GS, AX
   MOV   SS, AX
   RET

_ZN7AsmUtil3cliEv: ;void AsmUtil::cli();
    cli
    ret

_ZN7AsmUtil3stiEv: ;void AsmUtil::sti();
    sti
    ret

_ZN7AsmUtil3hltEv: ;void AsmUtil::hlt();
    hlt
    ret
