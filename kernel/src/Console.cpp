#include "Console.hpp"

size_t Console::current_column = 0;
size_t Console::current_line = 0;

int Console::getHeight() {
    return 25;
}

int Console::getWidth() {
    return 80;
}

void Console::clear() {
    for(int i = 0; i < Console::getWidth()*Console::getHeight()*2 ; ++i) {
        ((char*)0xb8000)[i] = 0;
    }
    current_column = 0;
    current_line = 0;	
}

int Console::printChar(char c) {
    return printChar(c, 0x07);
}

int Console::printChar(char c, uint8_t attr) {
	uint16_t* buffer = reinterpret_cast<uint16_t*>(0xb8000);
	if(c == '\b') {
		/*if (!input_buffer.empty()) {													//If butter is not empty pop the latest entry 
			input_buffer.pop()
		}*/
		if(current_column == 0 /* && !input_buffer.empty()*/) {
			if(current_line != 0) {
				current_column = 79;
				current_line--;
			}
		}
		else {
			current_column--;
		}
		buffer[current_line * 80 + current_column] = ' ';
		return 0;
	}
	else if(c == '\n') {
		//input_buffer.execute();														//Try to execute the content of the input buffer
		current_line++;
		current_column = 0;
		return 0;
	}
	/*if (!input_buffer.full()) {														//If buffer not full push another character
		input_buffer.push(c);
	}
	else {
		//input_buffer overflow error													//Print it on console or create some logging system 
	}*/
	buffer[current_line * 80 + current_column] = (uint16_t)c | (uint16_t)attr << 8;
	if(current_column < 79) {				
		current_column++;
	}
	else {
		current_line++;
		current_column = 0;
	}
	return 0;
}

int Console::printString(const char* string) {
	while (*string != '\0') {
		printChar(*string, 0x07);
		string++;
	}
	return 0;
}

int Console::printInt(unsigned int number, int base) {			//Niefektywne  w chuj, do poprawki, ale dziala i printuje inta
    char* buffer = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
    int index = 0;
    for (unsigned int i = number ; i >= base ; i /= base) ++index;
    buffer[index+1] = 0;
    while(index >= 0) {
        if(number%base < 10) {
            buffer[index] = number%base + '0';
        } else {
            buffer[index] = number%base + 'A' - 10;
        }
        number /= base;
        --index;
    }
    printString(buffer);
}
