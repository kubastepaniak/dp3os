#pragma once

#include "types.hpp"

#define IDT_SIZE 256
#define PIC_1_CTRL 0x20
#define PIC_2_CTRL 0xA0
#define PIC_1_DATA 0x21
#define PIC_2_DATA 0xA1

class IDTManager {
    typedef void(*FPIH)(uint8_t index); //FunctionPointerInterruptHandler
    public:
        struct IntDesc {
            uint16_t offset_lowerbits;
            uint16_t selector;
            uint8_t  zero;
            uint8_t  type_attr;
            uint16_t offset_higherbits;
        } __attribute__((packed));
        
        struct IDTlocator
        {
            uint16_t limit;
            uint32_t base;
        } __attribute__((packed));
        
    private:
        static const int idtSize = 256;
        static IntDesc IDT[idtSize];
        static FPIH intHandlers[idtSize];

    public:
        static void init();
        static void loadIDT();
        static IntDesc getIntDesc(uint8_t index);
        static void setIntDesc(uint8_t index, IntDesc descriptor);
        static void setIntDesc(uint8_t index, void(*isr)());
        static void interruptHandler(uint8_t index);
        static void registerIntHandler(uint8_t index, FPIH handler);
        static void unregisterIntHandler(uint8_t index);
    
};
