/*Function for the early allocations, we assume that the memory we allocate with this will never be retrieved*/
#ifndef KMALLOC_H
#define KMALLOC_H
#pragma once
#include "types.hpp"
#include "end.hpp"

uint32_t kmalloc(uint32_t size, int align, uint32_t *phys);
#endif


