#include "types.hpp"

#include "Vector.hpp"

template<class T> class PriorityQueue {
    private:
        Vector<T> items;
        void sort();
    public:
        PriorityQueue();
        void add(const T& item);
        void add(T&& item);
        T& getCurrent();
        void next();
        bool hasNext();
};

template<class T>
PriorityQueue<T>::PriorityQueue(unsigned int capacity = 0) : items(capacity) {}

template<class T>
void PriorityQueue<T>::add(const T& item) {
    items.push_back(item);
    sort();
}

template<class T>
void PriorityQueue<T>::add(T&& item) {
    items.push_back(item);
    sort();
}

template<class T>
T& PriorityQueue<T>::next() {
    return *(items.begin());
}

template<class T>
void PriorityQueue<T>::sort() {
	int i, j;
	for (i = 0; i < size - 1; i++) {
		for (j = 0; j < size - i - 1; j++) {
			if (items[j] > items[j + 1]) {
				T temp = *items[j];
				*items[j] = *items[j + 1];
				*items[j + 1] = temp;
			}
		}
	}
}

template<class T>
bool PriorityQueue<T>::hasNext() {
    return items.getSize() != 0;
}
