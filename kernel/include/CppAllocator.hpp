#pragma once
#include "types.hpp"

class CppAllocator {
    public:
        typedef void* (*NewFunction)(unsigned int size);
        typedef void (*DeleteFunction)(void* pointer);
        
        static void setNewFunction(NewFunction newFunction);
        static void setDeleteFunction(DeleteFunction deleteFunction);
        
        friend void* operator new(unsigned int size);
        friend void operator delete(void* pointer);
        
    private:
        static NewFunction newFunction;
        static DeleteFunction deleteFunction;
        

};
