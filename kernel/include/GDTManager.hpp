#pragma once

#include "types.hpp"

class GDTManager {
    public:        
        struct GDTlocator {
            uint16_t size;
            uint32_t offset;
        } __attribute__((packed));
        
        struct GDTentry {
            uint8_t limit_0_7;
            uint8_t limit_8_15;
            uint8_t base_0_7;
            uint8_t base_8_15;
            uint8_t  base_16_23;
            uint8_t  accessByte;
            uint8_t  flags_limit_16_19;
            uint8_t  base_24_31;
        } __attribute__((packed));
        
        struct simpleGDTentry {
            uint32_t base;
            uint32_t limit;
            uint8_t  accessByte;
            uint8_t  flags;
        } __attribute__((packed));
        
        static const uint8_t PRIVILEGE_0 = 0x00;
        static const uint8_t PRIVILEGE_1 = 0x20;
        static const uint8_t PRIVILEGE_2 = 0x40;
        static const uint8_t S_CODE_DATA = 0x10;
        static const uint8_t EXECUTABLE  = 0x08;
        static const uint8_t DIR_CONF    = 0x04;
        
    private:
        static const int gdtSize = 3;
        static GDTentry GDT[gdtSize];

    public:
        static void init();
        static GDTentry translateFromSimple(simpleGDTentry sGDTe);
    
};
