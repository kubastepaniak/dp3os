#pragma once
#include "types.hpp"
#include "Console.hpp"
#include "kmalloc.hpp"
#include "IDTManager.hpp"
#include "Util.hpp"


// Macros used in the bitset algorithms.
#define INDEX_FROM_BIT(a) (a/(8*4))
#define OFFSET_FROM_BIT(a) (a%(8*4))

extern uint32_t placement_address;

typedef struct page
{
	uint32_t present : 1;   // Page present in memory
	uint32_t rw : 1;   // Read-only if clear, readwrite if set
	uint32_t user : 1;   // Supervisor level only if clear
	uint32_t accessed : 1;   // Has the page been accessed since last refresh?
	uint32_t dirty : 1;   // Has the page been written to since last refresh?
	uint32_t unused : 7;   // Amalgamation of unused and reserved bits
	uint32_t frame : 20;  // Frame address (shifted right 12 bits)
} page_t;

typedef struct page_table
{
	page_t pages[1024];
} page_table_t;

typedef struct page_directory
{
	/**
	   Array of pointers to pagetables.
	**/
	page_table_t* tables[1024];
	/**
	   Array of pointers to the pagetables above, but gives their *physical*
	   location, for loading into the CR3 register.
	**/
	uint32_t tablesPhysical[1024];
	/**
	   The physical address of tablesPhysical. This comes into play
	   when we get our kernel heap allocated and the directory
	   may be in a different location in virtual memory.
	**/
	uint32_t physicalAddr;
} page_directory_t;

class PagingManager {
public:
	static void set_frame(uint32_t frame_addr);
	static void clear_frame(uint32_t frame_addr);
	static uint32_t test_frame(uint32_t frame_addr);
	static uint32_t first_frame();
	static void alloc_frame(page_t *page, int is_kernel, int is_writeable);	
        static 	void free_frame(page_t *page);
	static void initialise_paging(uint32_t mem_end);
	static void switch_page_directory(page_directory_t *dir);
	static page_t* get_page(uint32_t address, int make, page_directory_t *dir);
	static void page_fault(uint8_t fault_code);
	static page_directory_t* getCurrentDirectory();
private:
	static uint32_t* frames;
	static uint32_t nframes;
	static page_directory_t *kernel_directory;
	static page_directory_t *current_directory;
};



