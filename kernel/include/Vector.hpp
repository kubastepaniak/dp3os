template<typename T>
class Vector {
    private:
        unsigned int size;
        unsigned int capacity;
        T* array;
    
    public:
        Vector(unsigned int capacity = 0) : size(0), capacity(capacity) {
            array = new T[capacity];
        }
        ~Vector() {
            delete[] array;
        }
        
        T& operator=(Vector v) {
            
        }
        
        T* begin() {
            return array;
        }
        
        T* end() {
            return array+size;
        }
        
        void push_back(const T& element) {
            if(size == capacity) {
                grow();
            }
            new (array[size]) T(element);
            ++size;
        }
        
        void push_back(T&& element) {
            if(size == capacity) {
                grow();
            }
            new (array[size]) T(std::move(element));
            ++size;
        }
        
        void grow() {
            capacity *= 2;
            if(capacity == 0) capacity = 1;
            T* newArray = reinterpret_cast<T*>(new char[capacity*sizeof(T)]);
            for(unsigned int i = 0 ; i < size ; ++i) {
                newArray[i] = std::move(array[i]);
            }
            delete[] array;
            array = newArray;
        }
        
        unsigned int getCapacity() {
            return capacity;
        }
        
        unsigned int getSize() {
            return size;
        }
};
