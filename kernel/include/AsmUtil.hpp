#pragma once

#include "IDTManager.hpp"
#include "GDTManager.hpp"

class AsmUtil {
    private:
    
    public:
        static void loadGDT(GDTManager::GDTlocator*);
        static void loadIDT(IDTManager::IDTlocator*);
        static void storeGDT(GDTManager::GDTlocator*);
        static void storeIDT(IDTManager::IDTlocator*);
        static void reloadSegments();
        static void cli();
        static void sti();
        static void hlt();
};
