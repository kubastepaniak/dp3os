#pragma once

#include "types.hpp"

struct Console {
    public:
        static int getHeight();
        static int getWidth();
        static int printChar(char c);
        static int printChar(char c, uint8_t attr);
        static int printString(const char* string);
        static int printInt(unsigned int number, int base);
        static void clear();
    private:
        static size_t current_column;
        static size_t current_line;
};
