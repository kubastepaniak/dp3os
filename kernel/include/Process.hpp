#include "paging.hpp"

enum State {Active, Blocked, Idle, Ready, New};

typedef struct Context {
	uint32_t ebp;
	uint32_t edi;
	uint32_t esi;
	uint32_t edx;
	uint32_t ecx;
	uint32_t ebx;
	uint32_t eax;
	uint32_t eip;
} Context;

class Process {
private:
	char* name;
	int id;
	int priority;
	volatile Context* context;
	State state;
	Process* parent;
	page_directory_t* page_directory;
	void (*function)();
public:
	Process(char* name, int id, int priority, page_directory_t* page_directory, void (*function)(), State state = New, Process* parent = nullptr);
	Process* createChild();
	page_directory_t* getPageDirectory();
	volatile Context* getContext();
	int getPriority();
};

