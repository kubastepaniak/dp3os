#define SIZE 100

class CircularBuffer {
    private:
        char buffer[SIZE];
        unsigned int readIndex;
        unsigned int writeIndex;
        const unsigned int size;
    public:
        CircularBuffer();
        int writeByte(char byte);
        char readByte();
};
