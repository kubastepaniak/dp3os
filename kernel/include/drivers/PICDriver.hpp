#pragma once
#include "Driver.hpp"

#include "types.hpp"

class PICDriver : public Driver {
    typedef void(*FPIH)();
    private:
        static FPIH irqHandlers[16];
        static const uint8_t PIC1_COMMAND = 0x20;
        static const uint8_t PIC1_DATA    = 0x21;
        static const uint8_t PIC2_COMMAND = 0xA0;
        static const uint8_t PIC2_DATA    = 0xA1;
        static const uint8_t PIC1_IRQ0    = 0x20;
        static const uint8_t PIC2_IRQ8    = 0x28;
        static const uint8_t PIC_EOI      = 0x20;
        static void setIRQmask(uint8_t irq);
        static void clearIRQmask(uint8_t irq);

    public:
        static void init();
        static void IRQ(uint8_t index);
        static void registerIRQhandler(uint8_t irq, FPIH handler);
        static void unregisterIRQhandler(uint8_t irq);
};
