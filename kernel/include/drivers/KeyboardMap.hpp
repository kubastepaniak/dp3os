#pragma once

namespace KeyboardMap {
    const unsigned char lowercase[128] = {
        0,  27, '1', '2', '3', '4', '5', '6', '7', '8',     /* 9 */
      '9', '0', '-', '=', '\b',     /* Backspace */
      '\t',                 /* Tab */
      'q', 'w', 'e', 'r',   /* 19 */
      't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n', /* Enter key */
        0,                  /* 29   - Control */
      'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',     /* 39 */
     '\'', '`',   0,                /* Left shift */
     '\\', 'z', 'x', 'c', 'v', 'b', 'n',                    /* 49 */
      'm', ',', '.', '/',   0,                              /* Right shift */
      '*',
        0,  /* Alt */
      ' ',  /* Space bar */
        0,  /* Caps lock */
        0,  /* 59 - F1 key ... > */
        0,   0,   0,   0,   0,   0,   0,   0,
        0,  /* < ... F10 */
        0,  /* 69 - Num lock*/
        0,  /* Scroll Lock */
        0,  /* Home key */
        0,  /* Up Arrow */
        0,  /* Page Up */
      '-',
        0,  /* Left Arrow */
        0,
        0,  /* Right Arrow */
      '+',
        0,  /* 79 - End key*/
        0,  /* Down Arrow */
        0,  /* Page Down */
        0,  /* Insert Key */
        0,  /* Delete Key */
        0,   0,   0,
        0,  /* F11 Key */
        0,  /* F12 Key */
        0,  /* All other keys are undefined */
    };

    const unsigned char uppercase[128] = {
        0,  27, '!', '@', '#', '$', '%', '^','&','*', /* 9 */
      '(', ')', '_', '+', '\b', /* Backspace */
      '\t',         /* Tab */
      'Q', 'W', 'E', 'R',   /* 19 */
      'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '\n', /* Enter key */
        0,          /* 29   - Control */
      'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', /* 39 */
     '"', '|', 0, 0,       /* Left shift */
      'Z', 'X', 'C', 'V', 'B', 'N',            /* 49 */
      'M', '<', '>', '?',   0,              /* Right shift */
      '*',
        0,  /* Alt */
      ' ',  /* Space bar */
        0,  /* Caps lock */
        0,  /* 59 - F1 key ... > */
        0,   0,   0,   0,   0,   0,   0,   0,
        0,  /* < ... F10 */
        0,  /* 69 - Num lock*/
        0,  /* Scroll Lock */
        '7',
        '8',
        '9',
        '-',
        '4',
        '5',
        '6',
        '+',
        '1',
        '2',
        '3',
        '0',
        0,  /* Delete Key */
        0,   0,   0,
        0,  /* F11 Key */
        0,  /* F12 Key */
        0,  /* All other keys are undefined */
    };

    const char KEY_ENTER       = 0x1C;
    const char KEY_BACKSPACE   = 0x0E;
    const char KEY_UP          = 0x48;
    const char KEY_DOWN        = 0x50;
    const char KEY_LEFT_SHIFT  = 0x2A;
    const char KEY_RIGHT_SHIFT = 0x36;
    const char KEY_LEFT_CTRL   = 0x1D;
    const char KEY_ALT         = 56;
    const char KEY_F1          = 59;
    const char KEY_F2          = 60;
    const char KEY_F3          = 61;
}
