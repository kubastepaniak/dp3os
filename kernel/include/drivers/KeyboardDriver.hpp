#pragma once
#include "Driver.hpp"
#include "types.hpp"

class KeyboardDriver : public Driver {
    typedef void (*FPUH)(uint8_t keycode);
    private:
        static FPUH userHandler;
        static const uint16_t dataPort = 0x60;
        static const uint16_t commandPort = 0x64;

    public:
        static void init(FPUH handler);
        static void keyboardEventHandler();
};
