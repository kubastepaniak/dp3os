#pragma once

#include "PICDriver.hpp"
#include "CPUIO.hpp"

class PITDriver {
	typedef void(*FPIH)();
private:
	static int frequency;
	static FPIH pit_handler;
public:
	static void init(int frequency, FPIH handler);
};

