#pragma once
#include "types.hpp"

class MemoryBlock {
    public:
        uint32_t baseAddress;
        uint32_t length;
        
        MemoryBlock();
        MemoryBlock(uint32_t baseAddress, uint32_t length);
        operator void*();

};
