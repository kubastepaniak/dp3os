#pragma once

#include "types.hpp"

class CPUIO {
    private:

    public:
        static uint8_t readPort(uint16_t port);
        static void writePort(uint16_t port, uint8_t data);
};
