#include "Process.hpp"
#include "PriorityQueue.hpp"
#include "PITDriver.hpp"
#include "AsmUtil.hpp"

class Scheduler {
private:
	static PriorityQueue<Process>* processes;
	static void idleTask();
public: 
	Scheduler();
	static void init();
	void switchToProcess(Process* process);
	static PriorityQueue<Process>* getProcesses();
	static void switchToNext();
};

void switch_process(volatile Context* previous, volatile Context* next);

extern "C" {
	uint32_t get_context_address(uint32_t pid);
	uint32_t get_process_cr3(uint32_t pid);
}
